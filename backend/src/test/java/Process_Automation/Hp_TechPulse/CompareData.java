package Process_Automation.Hp_TechPulse;

import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class CompareData extends CommonMethod {

	public static void main(String[] args) {
		try {

			ArrayList<String> names = new ArrayList<String>();
			String[] pathnames;

			// Creates a new File instance by converting the given pathname string into an abstract pathname
			File f = new File(ConstantPath.DIRECTORY_PATH);
			File finalsheet = new File(ConstantPath.EXCEL_PATH_SUBTYPE);

			// Populates the array with names of files and directories
			pathnames = f.list();
			long count = pathnames.length;
			System.out.println("Toatal number of sheets to compare : " + count);

			// For each pathname in the pathnames array
			for (String pathname : pathnames) {
				// Print the names of files and directories
				names.add(pathname);
			}

			for (int i = 0; i < pathnames.length; i++) {
				// get input excel files
				FileInputStream excellFile1 = new FileInputStream(new File(ConstantPath.EXCEL_PATH_SUBTYPE));
				FileInputStream excellFile2 = new FileInputStream(new File(ConstantPath.DIRECTORY_PATH + names.get(i)));
				System.out.println(ConstantPath.EXCEL_PATH_SUBTYPE);
				System.out.println(ConstantPath.DIRECTORY_PATH + names.get(i));

				// Create Workbook instance holding reference to .xlsx file
				XSSFWorkbook workbook1 = new XSSFWorkbook(excellFile1);
				XSSFWorkbook workbook2 = new XSSFWorkbook(excellFile2);

				// Get first/desired sheet from the workbook
				XSSFSheet sheet1 = workbook1.getSheetAt(0);
				XSSFSheet sheet2 = workbook2.getSheetAt(0);

				System.out.println("sheetname " + names.get(i));
				compareCost(sheet1, sheet2, names.get(i));

				// close files
				excellFile1.close();
				excellFile2.close();

				FileOutputStream finalexcellFile = new FileOutputStream(new File(ConstantPath.EXCEL_PATH_SUBTYPE));
				workbook1.write(finalexcellFile);
				finalexcellFile.close();

			}
			Desktop.getDesktop().browse(finalsheet.toURI());
			ExcelToImage.generateImages(ConstantPath.EXCEL_PATH_SUBTYPE, ConstantPath.JSON_PATH_SUBTYPE);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}