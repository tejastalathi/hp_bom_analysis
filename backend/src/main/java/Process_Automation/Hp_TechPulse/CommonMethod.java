package Process_Automation.Hp_TechPulse;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;

public class CommonMethod {
	// Compare Two Costs
	public static void compareCost(XSSFSheet sheet1, XSSFSheet sheet2, String sheetname) throws IOException {
		int firstRow1 = sheet1.getFirstRowNum();
		int lastRow1 = sheet1.getLastRowNum();
		int minCost = 0, maxCost = 0, minCostVendorName = 0;

		XSSFRow header1 = sheet1.getRow(0);
		int firstCell1 = header1.getFirstCellNum();
		int lastCell1 = header1.getLastCellNum();

		// Compare all cells in a row
		for (int i = firstCell1; i <= lastCell1; i++) {
			XSSFRow row0 = sheet1.getRow(6);
			if (row0.getCell(i).getStringCellValue().equals("MinCost")) {
				minCost = i;
			}

			if (row0.getCell(i).getStringCellValue().equals("MaxCost")) {
				maxCost = i;
			}

			if (row0.getCell(i).getStringCellValue().trim().equals("Vendor of Min-Cost")) {
				minCostVendorName = i;
			}
		}

		for (int j = 7; j < lastRow1 - 1; j++) {
			XSSFRow row1 = sheet1.getRow(j);
			XSSFRow row2 = sheet2.getRow(j);

			XSSFCell costCell1 = row1.getCell(minCost);
			XSSFCell costCell2 = row2.getCell(minCost);
			XSSFCell vendorCell1 = row1.getCell(minCostVendorName);
			XSSFCell maxCostCell1 = row1.getCell(maxCost);

			double costchk = 0.0;

			if (Double.compare(costCell1.getNumericCellValue(), costchk) == 0) {
				costCell1.setCellValue(costCell2.getNumericCellValue());
				vendorCell1.setCellValue(sheetname.split("\\.")[0]);
				maxCostCell1.setCellValue(costCell2.getNumericCellValue());
			} else if (costCell1.getNumericCellValue() != costchk) {
				if (costCell1.getNumericCellValue() > costCell2.getNumericCellValue()) {
					costCell1.setCellValue(costCell2.getNumericCellValue());
					vendorCell1.setCellValue(sheetname.split("\\.")[0]);
				}
			}

			if (costCell1.getNumericCellValue() != costchk) {
				if (costCell1.getNumericCellValue() > costCell2.getNumericCellValue() && costCell1.getNumericCellValue() > maxCostCell1.getNumericCellValue()) {
					maxCostCell1.setCellValue(costCell1.getNumericCellValue());
				} else if (costCell2.getNumericCellValue() > maxCostCell1.getNumericCellValue()) {
					maxCostCell1.setCellValue(costCell2.getNumericCellValue());
				} else {
					maxCostCell1.setCellValue(maxCostCell1.getNumericCellValue());
				}
			}
		}
	}
}
