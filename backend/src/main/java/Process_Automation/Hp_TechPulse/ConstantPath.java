package Process_Automation.Hp_TechPulse;

import java.io.File;

public class ConstantPath {
	public static final String EXCEL_PATH_SUBTYPE = System.getProperty("user.dir") + File.separator + "FinalTestData" + File.separator + "FinalExcelSheet.xlsx";
	public static final String DIRECTORY_PATH = System.getProperty("user.dir") + File.separator + "SubTypeTestData" + File.separator;
	public static final String CSV_PATH_SUBTYPE = System.getProperty("user.dir") + File.separator + "convertedCSVFile.csv";
	public static final String JSON_PATH_SUBTYPE = System.getProperty("user.dir") + File.separator + "../frontend/src/assets/final.jpeg";
}
