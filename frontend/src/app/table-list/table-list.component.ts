import { Component, OnInit } from '@angular/core';
// import { writeFileSync, readFileSync } from 'fs';
// import * as path from 'path';
// import a from"../Sheet1.json";
// import { readFileSync } from 'fs';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';

import { HttpModule } from '@angular/http';


@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.css']
})
export class TableListComponent implements OnInit {
  dataObj = [];

  dataHeader = {};
  showSubCategory = false;
  constructor(private http: HttpClient) { }

  ngOnInit() {


    this.http.get('../assets/Sheet1.json')
      .subscribe(data => {

        this.dataObj = JSON.stringify(data) as any;
        this.dataObj = JSON.parse(this.dataObj as any);
        this.dataHeader = this.dataObj[0];
        console.log(this.dataHeader);
        this.dataObj = this.dataObj.slice(1);

      })

  }
  readFile() {

  }

  openSubcategoryPage() {
    // window.open('../assets/final.jpeg' );
    if (this.showSubCategory)
      this.showSubCategory = false;
    else
      this.showSubCategory = true;
  }

  opencategoryPage() {
    //TODO change path of html
    //window.open('../assets/UiView.html' );
  }

  openFinalDeal() {
    //TODO change path of html
    //window.open('../assets/UiView.html' );
  }

}
