
export class CohortInputs {
    public cohortType:number;
    public timeType: number;
    public numOfDays:number;
    public paramValue :string
    public startDate   :string
	
    constructor() {
        this.cohortType=0;
        this.timeType = 0;
        this.numOfDays=30;
        this.paramValue='';
        this.startDate='';
    }
}